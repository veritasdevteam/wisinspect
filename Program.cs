﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisInspect
{
    class Program
    {
        static void Main(string[] args)
        {


             long ClaimID =0;
             long InspectNo = 0;
             string sType="";
             string CONN="";
             string SQL="";
             string sRequesterName;
             string sRequesterExt;
             string sRequesterEMail;
             long lContractID;
             string sContractHolder;
             string sContractSaleDate;
             string sContractSaleMile;
             string sVehYear;
             string sVehMake;
             string sVehModel;
             string sVIN;
             string sMileage;
             string sContractNo;
             string sAuthNo;
             string sRepairSite;
             string[] sIR;
             string sInspectType;
             string sAddr1;
             string sAddr2;
             string sCity;
             string sState;
             string sZip;
             string sPhone;
             string sContact;
             long lServiceCenterID;
             string sRespBack;
             long lRequestBy;
             long lProgramID;
             string sReason1="";
             string sReason2="";
             string sReason3="";

            for (int cnt = 0; cnt < args.Length; cnt++)
            {
                if (args[cnt] == "-claimid")
                {
                    ClaimID = Convert.ToInt32(args[cnt + 1]);
                }
                if (args[cnt] == "-inspectno")
                {
                    InspectNo = Convert.ToInt32(args[cnt + 1]);
                }
                if (args[cnt] == "-inspecttype")
                {
                    sType = args[cnt + 1];
                }
            }
            CONN = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";

            OpenClaimInspect();

             void OpenClaimInspect()
                {

                    SQL = "select * from claiminspection ";
                    SQL = SQL + "where claimid = " + ClaimID + " ";
                    if (InspectNo > 0)
                    {
                        SQL = SQL + "and inspectno = " + InspectNo + " ";
                    }
                    DBO.clsDBO clCI = new DBO.clsDBO();
                    clCI.OpenDB(SQL, CONN);
                    if (clCI.RowCount > 0)
                    {
                        clCI.GetRow();
                        if (clCI.get_Fields("detailsurl").Length > 0)
                        {
                            return;
                        }
                        clCI.GetRow();
                        if (clCI.get_Fields("inspectionid").Length > 0)
                        {
                            return;
                        }
                        lRequestBy = Convert.ToInt32(clCI.get_Fields("requestby"));
                        sReason1 = clCI.get_Fields("inspectnote");
                        OpenUserInfo();
                    }
                }
                 void OpenUserInfo()
                {
                    SQL = "select * from userinfo ";
                    SQL = SQL + "where userid = " + lRequestBy;
                    DBO.clsDBO clUI = new DBO.clsDBO();
                    clUI.OpenDB(SQL, CONN);
                    if (clUI.RowCount > 0)
                    {
                        clUI.GetRow();
                        sRequesterName = clUI.get_Fields("fname") + " " + clUI.get_Fields("lname");
                        sRequesterExt = clUI.get_Fields("phoneext");
                        sRequesterEMail = clUI.get_Fields("email");
                        GetClaimInfo();
                    }
                }

                 void GetClaimInfo()
                {
                    SQL = "select * from claim ";
                    SQL = SQL + "where claimid = " + ClaimID;
                    DBO.clsDBO clCL = new DBO.clsDBO();
                    clCL.OpenDB(SQL, CONN);
                    if (clCL.RowCount > 0)
                    {
                        clCL.GetRow();
                        lContractID = Convert.ToInt32(clCL.get_Fields("contractid"));
                        sMileage = clCL.get_Fields("lossmile");
                        sAuthNo = clCL.get_Fields("claimno");
                        sContact = clCL.get_Fields("sccontactinfo");
                        lServiceCenterID = Convert.ToInt32(clCL.get_Fields("servicecenterid"));
                        GetContractInfo();
                    }
                }

                 void GetContractInfo()
                {
                    SQL = "select * from contract ";
                    SQL = SQL + "where contractid = " + lContractID;
                    DBO.clsDBO clC = new DBO.clsDBO();
                    clC.OpenDB(SQL, CONN);
                    if (clC.RowCount > 0)
                    {
                        clC.GetRow();
                        sContractHolder = clC.get_Fields("fname") + " " + clC.get_Fields("lname");
                        sContractSaleDate = clC.get_Fields("saledate");
                        sContractSaleMile = clC.get_Fields("salemile");
                        sVehMake = clC.get_Fields("make");
                        sVehModel = clC.get_Fields("model");
                        sVehYear = clC.get_Fields("year");
                        sVIN = clC.get_Fields("vin");
                        sContractNo = clC.get_Fields("contractno");
                        lProgramID = Convert.ToInt32(clC.get_Fields("ProgramID"));
                        
                        CalcInspectType();
                        CalcInspectReason();
                        GetServiceCenter();
                    }
                }

                 void CalcInspectType()
                {
                    sInspectType = "Automotive";
                    if (lProgramID == 10)
                    {
                        sInspectType = "RV";
                    }
                    if (lProgramID == 48)
                    {
                        sInspectType = "RV";
                    }
                    if (lProgramID == 63)
                    {
                        sInspectType = "RV";
                    }
                    if (lProgramID == 9)
                    {
                        sInspectType = "Motorcycle/ATV";
                    }
                    if (lProgramID == 62)
                    {
                        sInspectType = "Motorcycle/ATV";
                    }
                }

                 void CalcInspectReason()
                {
                    SQL = "select * from claimnote ";
                    SQL = SQL + "where claimid = " + ClaimID + " ";
                    SQL = SQL + "and (claimnotetypeid = 1 ";
                    SQL = SQL + "or claimnotetypeid = 3 ";
                    SQL = SQL + "or claimnotetypeid = 5) ";
                    SQL = SQL + "order by claimnotetypeid ";
                    DBO.clsDBO clCN = new DBO.clsDBO();
                    clCN.OpenDB(SQL, CONN);
                    if (clCN.RowCount > 0)
                    {
                        for (int cnt = 0; cnt < clCN.RowCount; cnt++)
                        {
                            clCN.GetRowNo(cnt);
                            if (clCN.get_Fields("claimnotetypeid") == "1")
                            {
                                sReason2 = clCN.get_Fields("note");
                            }
                            if (clCN.get_Fields("claimnotetypeid") == "2")
                            {
                                sReason3 = clCN.get_Fields("note");
                            }
                        }
                    }
                    sIR = new string[]
                    {
                   sReason1,sReason2,sReason3
                   };
                }


                 void GetServiceCenter()
                {
                    SQL = "select * from servicecenter ";
                    SQL = SQL + "where servicecenterid = " + lServiceCenterID + " ";
                    DBO.clsDBO clSC = new DBO.clsDBO();
                    clSC.OpenDB(SQL, CONN);
                    if (clSC.RowCount > 0)
                    {
                        clSC.GetRow();
                        sRepairSite = clSC.get_Fields("servicecentername");
                        sAddr1 = clSC.get_Fields("addr1");
                        sAddr2 = clSC.get_Fields("addr2");
                        sCity = clSC.get_Fields("city");
                        sState = clSC.get_Fields("state");
                        sZip = clSC.get_Fields("zip");
                        sPhone = clSC.get_Fields("phone");
                        var Service = new WIS.wiswebserviceSoapClient();
                        var AuthUser = new WIS.AuthenticateHeader
                        {
                            Username = "redshield",
                            Password = "rs@@p120gh"
                        };

                        var sResp = Service.SendRequestB(AuthUser, sRequesterName, sRequesterExt, sRequesterEMail, sContractHolder, Convert.ToDateTime(sContractSaleDate),
                       sContractSaleMile, sVehYear, sVehMake, sVehModel, sMileage, sVIN, sContractNo, sAuthNo, sInspectType, sIR, sRepairSite, sAddr1,
                       sAddr2, sCity, sState, sZip, sPhone, sContact);
                        sRespBack = sResp.ToString();
                        AddInspectionID();
                    }
                }

                 void AddInspectionID()
                {
                    if (sRespBack.Length > 10)
                    {
                        return;
                    }
                    SQL = "update claiminspection ";
                    SQL = SQL + "set inspectionid = '" + sRespBack + "' ";
                    SQL = SQL + "where claimid = " + ClaimID + " ";
                    if (InspectNo > 0)
                    {
                        SQL = SQL + "and inspectno = " + InspectNo + " ";
                    }
                    DBO.clsDBO clR = new DBO.clsDBO();
                    clR.RunSQL(SQL, CONN);
                }

        }

    }
}
